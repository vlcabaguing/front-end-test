#Frontend Skill Test

## Tasks
1. Convert design.png to working webpage, including the modal (desktop only)
2. Integrate Recipe API on the page.
3. Apply Routing for the Home, Recipe, and Contact Us pages, display a dummy page just to show that the routing is working
4. Use Redux Toolkit for state management.
5. Use Lato as fontstyle
6. Apply CSS Modules for styling
 
## Goals

For this project we've provided design and recipe API with two endpoints. With these you'll need to complete the following objectives.
- Create a react application using your chosen CLI e.g create-react-app or vite
 
- Pull the data from the API. You can use this endpoint to get data from for this project:
            1. https://sbctechnicalexamapi-default-rtdb.firebaseio.com/recipes.json
            2. https://sbctechnicalexamapi-default-rtdb.firebaseio.com/specials.json

  OR This project includes a API server (json-server) for you to use. To install and run, use the commands below:

            - `npm i`
            - `npm run start:api`
          
Once running, you can use the API endpoints listed in the following section from `http://localhost:3001`. More information about querying the server can be found on the [json-server github page](https://github.com/typicode/json-server).

- Assets are provided in this folder : "public/images"


- Display all the recipes on the page and open the recipe detail on a modal using the provided design
- Get the data of the modal based from the selected item from the list.
- Ingredients with a matching `ingredientId` listed in the specials response should also show the special `title` and `text` under the ingredient name(See modal-instructions.jpg)

#### Endpoints & Schema

##### GET `/recipes`

```JSON
[
  Recipe {
    uuid: String
    title: String
    description: String
    images: {
      full: String
      medium: String,
      small: String
    }
    servings: Number
    prepTime: Number
    cookTime: Number
    postDate: Date
    editDate: Date
    ingredients: [
      Ingredient {
        uuid: String
        amount: Number
        measurement: String
        name: String
      }
    ]
    directions: [
      Direction {
        instructions: String
        optional: Boolean
      }
    ]
  }
]
```

##### GET `/specials`

Specials on ingredients

```json
[
  Special {
    uuid: String
    ingredientId: String
    type: String
    title: String
    geo: String (optional)
    text: String (optional)
  }
]
```
